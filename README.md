# Spike Report

## SPIKE TITLE - C++ & Unreal Engine

### Introduction

This spike report details how C++ in used in concert with Unreal Engine 4 and how to take advantage of Unreal Engine's leveragable API system.

### Goals
- Develop a "Launch-pad" actor using C++ programming, this actor should cause the player to be launched in the direction of the arrow.
- The Launchpad should also play a sound upon launching the player.
- Learn how to set the launch velocity as a variable that allows us to place multiple launch-pads in the scene, each with varying launch velocities.
- Develop our own deliverable to accompany the launchpad (My deliverable will be a point light that toggles on/off whenever the player walks near it.)

### Personnel

* Primary - Matthew Louden
* Secondary - N/A

### Technologies, Tools, and Resources used

* [updated referencing guide] (https://docs.unrealengine.com/en-US/Programming/UnrealBuildSystem/IWYUReferenceGuide/index.html)
* [C++ Programming Tutorials] (https://docs.unrealengine.com/en-US/Programming/Tutorials/index.html)
* [Variables, Timers & Events] (https://docs.unrealengine.com/en-US/Programming/Tutorials/VariablesTimersEvents/index.html)
* [Player Input & Pawns] (https://docs.unrealengine.com/en-US/Programming/Tutorials/PlayerInput/index.html)
* [Intro to C++ in UE4] (https://docs.unrealengine.com/en-US/Programming/Introduction/index.html)
* [C++ Class Creation Guide] (https://docs.unrealengine.com/en-US/Gameplay/ClassCreation/CodeOnly/index.html)
* [C++ & Blueprints] (https://docs.unrealengine.com/en-US/Gameplay/ClassCreation/CodeAndBlueprints/index.html)
* [Lightswitch tutorial] (https://docs.unrealengine.com/en-US/Gameplay/ClassCreation/CodeOnly/index.html)

### Tasks Undertaken

Show only the key tasks that will help the developer/reader understand what is required.

1. Generated New Visual Studio C++ Script
1. Added new C++ Actor (Launchpad)
	2. Inserted code into C++ actor to [generate a cube for collision] (https://answers.unrealengine.com/questions/142180/how-to-spawn-a-simple-cube-in-c.html)
	2. Inserted code into C++ actor to generate a visual arrow component
	2. Inserted code into C++ actor to [generate an 'on overlap' event] (https://unrealcpp.com/on-overlap-begin/)
	2. Inserted code into C++ actor to launch the character when it overlaps with the collision cube
	2. Inserted code into C++ actor to [launch the character in the direction of the generated arrow component] (https://www.reddit.com/r/unrealengine/comments/951ib5/question_beginner_c_adding_a_force_to_my_actor/)
		3. Made the launch power into a variable
		3. Generate a new [sound file to be triggered when the character overlaps with the collision cube] (https://answers.unrealengine.com/questions/66132/how-to-play-an-ambient-sound-in-c.html)
1. Added new C++ Actor (Proximity Light)
	2. Inserted code into C++ actor to generate a sphere for collision
	2. Inserted code into C++ actor to generate a point light
	2. Inserted code into C++ actor to generate an 'on overlap' event
	2. Inserted code into C++ actor to [toggle the pointlight when the sphere overlaps with the character] (https://docs.unrealengine.com/en-US/Gameplay/ClassCreation/CodeOnly/index.html)

### What we found out
- We found out how to develop an actor using C++ programming and how to make this C++ actor interact with other objects in the scene (through launching them).
- We found out how to make C++ values into publicly editable variables for per-instance use.
