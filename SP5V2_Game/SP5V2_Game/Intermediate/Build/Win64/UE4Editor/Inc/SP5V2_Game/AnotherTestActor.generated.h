// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef SP5V2_GAME_AnotherTestActor_generated_h
#error "AnotherTestActor.generated.h already included, missing '#pragma once' in AnotherTestActor.h"
#endif
#define SP5V2_GAME_AnotherTestActor_generated_h

#define SP5V2_Game_Source_SP5V2_Game_AnotherTestActor_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnCubeBeginOverlap) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_GET_UBOOL(Z_Param_bFromSweep); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnCubeBeginOverlap(Z_Param_OverlappedComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult); \
		P_NATIVE_END; \
	}


#define SP5V2_Game_Source_SP5V2_Game_AnotherTestActor_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnCubeBeginOverlap) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_GET_UBOOL(Z_Param_bFromSweep); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnCubeBeginOverlap(Z_Param_OverlappedComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult); \
		P_NATIVE_END; \
	}


#define SP5V2_Game_Source_SP5V2_Game_AnotherTestActor_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAAnotherTestActor(); \
	friend struct Z_Construct_UClass_AAnotherTestActor_Statics; \
public: \
	DECLARE_CLASS(AAnotherTestActor, AStaticMeshActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SP5V2_Game"), NO_API) \
	DECLARE_SERIALIZER(AAnotherTestActor)


#define SP5V2_Game_Source_SP5V2_Game_AnotherTestActor_h_18_INCLASS \
private: \
	static void StaticRegisterNativesAAnotherTestActor(); \
	friend struct Z_Construct_UClass_AAnotherTestActor_Statics; \
public: \
	DECLARE_CLASS(AAnotherTestActor, AStaticMeshActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SP5V2_Game"), NO_API) \
	DECLARE_SERIALIZER(AAnotherTestActor)


#define SP5V2_Game_Source_SP5V2_Game_AnotherTestActor_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAnotherTestActor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAnotherTestActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAnotherTestActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAnotherTestActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAnotherTestActor(AAnotherTestActor&&); \
	NO_API AAnotherTestActor(const AAnotherTestActor&); \
public:


#define SP5V2_Game_Source_SP5V2_Game_AnotherTestActor_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAnotherTestActor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAnotherTestActor(AAnotherTestActor&&); \
	NO_API AAnotherTestActor(const AAnotherTestActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAnotherTestActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAnotherTestActor); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAnotherTestActor)


#define SP5V2_Game_Source_SP5V2_Game_AnotherTestActor_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ImpulseForce() { return STRUCT_OFFSET(AAnotherTestActor, ImpulseForce); }


#define SP5V2_Game_Source_SP5V2_Game_AnotherTestActor_h_15_PROLOG
#define SP5V2_Game_Source_SP5V2_Game_AnotherTestActor_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SP5V2_Game_Source_SP5V2_Game_AnotherTestActor_h_18_PRIVATE_PROPERTY_OFFSET \
	SP5V2_Game_Source_SP5V2_Game_AnotherTestActor_h_18_RPC_WRAPPERS \
	SP5V2_Game_Source_SP5V2_Game_AnotherTestActor_h_18_INCLASS \
	SP5V2_Game_Source_SP5V2_Game_AnotherTestActor_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SP5V2_Game_Source_SP5V2_Game_AnotherTestActor_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SP5V2_Game_Source_SP5V2_Game_AnotherTestActor_h_18_PRIVATE_PROPERTY_OFFSET \
	SP5V2_Game_Source_SP5V2_Game_AnotherTestActor_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	SP5V2_Game_Source_SP5V2_Game_AnotherTestActor_h_18_INCLASS_NO_PURE_DECLS \
	SP5V2_Game_Source_SP5V2_Game_AnotherTestActor_h_18_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class AnotherTestActor."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SP5V2_GAME_API UClass* StaticClass<class AAnotherTestActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SP5V2_Game_Source_SP5V2_Game_AnotherTestActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
