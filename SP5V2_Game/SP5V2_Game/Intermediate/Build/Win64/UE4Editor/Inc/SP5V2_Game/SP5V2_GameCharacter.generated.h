// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SP5V2_GAME_SP5V2_GameCharacter_generated_h
#error "SP5V2_GameCharacter.generated.h already included, missing '#pragma once' in SP5V2_GameCharacter.h"
#endif
#define SP5V2_GAME_SP5V2_GameCharacter_generated_h

#define SP5V2_Game_Source_SP5V2_Game_SP5V2_GameCharacter_h_14_RPC_WRAPPERS
#define SP5V2_Game_Source_SP5V2_Game_SP5V2_GameCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define SP5V2_Game_Source_SP5V2_Game_SP5V2_GameCharacter_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASP5V2_GameCharacter(); \
	friend struct Z_Construct_UClass_ASP5V2_GameCharacter_Statics; \
public: \
	DECLARE_CLASS(ASP5V2_GameCharacter, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SP5V2_Game"), NO_API) \
	DECLARE_SERIALIZER(ASP5V2_GameCharacter)


#define SP5V2_Game_Source_SP5V2_Game_SP5V2_GameCharacter_h_14_INCLASS \
private: \
	static void StaticRegisterNativesASP5V2_GameCharacter(); \
	friend struct Z_Construct_UClass_ASP5V2_GameCharacter_Statics; \
public: \
	DECLARE_CLASS(ASP5V2_GameCharacter, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SP5V2_Game"), NO_API) \
	DECLARE_SERIALIZER(ASP5V2_GameCharacter)


#define SP5V2_Game_Source_SP5V2_Game_SP5V2_GameCharacter_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASP5V2_GameCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASP5V2_GameCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASP5V2_GameCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASP5V2_GameCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASP5V2_GameCharacter(ASP5V2_GameCharacter&&); \
	NO_API ASP5V2_GameCharacter(const ASP5V2_GameCharacter&); \
public:


#define SP5V2_Game_Source_SP5V2_Game_SP5V2_GameCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASP5V2_GameCharacter(ASP5V2_GameCharacter&&); \
	NO_API ASP5V2_GameCharacter(const ASP5V2_GameCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASP5V2_GameCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASP5V2_GameCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASP5V2_GameCharacter)


#define SP5V2_Game_Source_SP5V2_Game_SP5V2_GameCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Mesh1P() { return STRUCT_OFFSET(ASP5V2_GameCharacter, Mesh1P); } \
	FORCEINLINE static uint32 __PPO__FP_Gun() { return STRUCT_OFFSET(ASP5V2_GameCharacter, FP_Gun); } \
	FORCEINLINE static uint32 __PPO__FP_MuzzleLocation() { return STRUCT_OFFSET(ASP5V2_GameCharacter, FP_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__VR_Gun() { return STRUCT_OFFSET(ASP5V2_GameCharacter, VR_Gun); } \
	FORCEINLINE static uint32 __PPO__VR_MuzzleLocation() { return STRUCT_OFFSET(ASP5V2_GameCharacter, VR_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__FirstPersonCameraComponent() { return STRUCT_OFFSET(ASP5V2_GameCharacter, FirstPersonCameraComponent); } \
	FORCEINLINE static uint32 __PPO__R_MotionController() { return STRUCT_OFFSET(ASP5V2_GameCharacter, R_MotionController); } \
	FORCEINLINE static uint32 __PPO__L_MotionController() { return STRUCT_OFFSET(ASP5V2_GameCharacter, L_MotionController); } \
	FORCEINLINE static uint32 __PPO__TriggerCapsule() { return STRUCT_OFFSET(ASP5V2_GameCharacter, TriggerCapsule); }


#define SP5V2_Game_Source_SP5V2_Game_SP5V2_GameCharacter_h_11_PROLOG
#define SP5V2_Game_Source_SP5V2_Game_SP5V2_GameCharacter_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SP5V2_Game_Source_SP5V2_Game_SP5V2_GameCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	SP5V2_Game_Source_SP5V2_Game_SP5V2_GameCharacter_h_14_RPC_WRAPPERS \
	SP5V2_Game_Source_SP5V2_Game_SP5V2_GameCharacter_h_14_INCLASS \
	SP5V2_Game_Source_SP5V2_Game_SP5V2_GameCharacter_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SP5V2_Game_Source_SP5V2_Game_SP5V2_GameCharacter_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SP5V2_Game_Source_SP5V2_Game_SP5V2_GameCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	SP5V2_Game_Source_SP5V2_Game_SP5V2_GameCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	SP5V2_Game_Source_SP5V2_Game_SP5V2_GameCharacter_h_14_INCLASS_NO_PURE_DECLS \
	SP5V2_Game_Source_SP5V2_Game_SP5V2_GameCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SP5V2_GAME_API UClass* StaticClass<class ASP5V2_GameCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SP5V2_Game_Source_SP5V2_Game_SP5V2_GameCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
