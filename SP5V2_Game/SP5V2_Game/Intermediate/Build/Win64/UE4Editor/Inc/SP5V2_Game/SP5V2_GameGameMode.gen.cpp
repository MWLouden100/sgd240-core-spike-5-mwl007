// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SP5V2_Game/SP5V2_GameGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSP5V2_GameGameMode() {}
// Cross Module References
	SP5V2_GAME_API UClass* Z_Construct_UClass_ASP5V2_GameGameMode_NoRegister();
	SP5V2_GAME_API UClass* Z_Construct_UClass_ASP5V2_GameGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_SP5V2_Game();
// End Cross Module References
	void ASP5V2_GameGameMode::StaticRegisterNativesASP5V2_GameGameMode()
	{
	}
	UClass* Z_Construct_UClass_ASP5V2_GameGameMode_NoRegister()
	{
		return ASP5V2_GameGameMode::StaticClass();
	}
	struct Z_Construct_UClass_ASP5V2_GameGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ASP5V2_GameGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_SP5V2_Game,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASP5V2_GameGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "SP5V2_GameGameMode.h" },
		{ "ModuleRelativePath", "SP5V2_GameGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ASP5V2_GameGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ASP5V2_GameGameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ASP5V2_GameGameMode_Statics::ClassParams = {
		&ASP5V2_GameGameMode::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802A8u,
		METADATA_PARAMS(Z_Construct_UClass_ASP5V2_GameGameMode_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ASP5V2_GameGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ASP5V2_GameGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ASP5V2_GameGameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASP5V2_GameGameMode, 1973644807);
	template<> SP5V2_GAME_API UClass* StaticClass<ASP5V2_GameGameMode>()
	{
		return ASP5V2_GameGameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASP5V2_GameGameMode(Z_Construct_UClass_ASP5V2_GameGameMode, &ASP5V2_GameGameMode::StaticClass, TEXT("/Script/SP5V2_Game"), TEXT("ASP5V2_GameGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASP5V2_GameGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
