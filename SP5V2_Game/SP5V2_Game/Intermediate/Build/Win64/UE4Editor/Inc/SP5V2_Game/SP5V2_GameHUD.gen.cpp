// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SP5V2_Game/SP5V2_GameHUD.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSP5V2_GameHUD() {}
// Cross Module References
	SP5V2_GAME_API UClass* Z_Construct_UClass_ASP5V2_GameHUD_NoRegister();
	SP5V2_GAME_API UClass* Z_Construct_UClass_ASP5V2_GameHUD();
	ENGINE_API UClass* Z_Construct_UClass_AHUD();
	UPackage* Z_Construct_UPackage__Script_SP5V2_Game();
// End Cross Module References
	void ASP5V2_GameHUD::StaticRegisterNativesASP5V2_GameHUD()
	{
	}
	UClass* Z_Construct_UClass_ASP5V2_GameHUD_NoRegister()
	{
		return ASP5V2_GameHUD::StaticClass();
	}
	struct Z_Construct_UClass_ASP5V2_GameHUD_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ASP5V2_GameHUD_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AHUD,
		(UObject* (*)())Z_Construct_UPackage__Script_SP5V2_Game,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASP5V2_GameHUD_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Rendering Actor Input Replication" },
		{ "IncludePath", "SP5V2_GameHUD.h" },
		{ "ModuleRelativePath", "SP5V2_GameHUD.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ASP5V2_GameHUD_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ASP5V2_GameHUD>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ASP5V2_GameHUD_Statics::ClassParams = {
		&ASP5V2_GameHUD::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008002ACu,
		METADATA_PARAMS(Z_Construct_UClass_ASP5V2_GameHUD_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ASP5V2_GameHUD_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ASP5V2_GameHUD()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ASP5V2_GameHUD_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASP5V2_GameHUD, 2617140188);
	template<> SP5V2_GAME_API UClass* StaticClass<ASP5V2_GameHUD>()
	{
		return ASP5V2_GameHUD::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASP5V2_GameHUD(Z_Construct_UClass_ASP5V2_GameHUD, &ASP5V2_GameHUD::StaticClass, TEXT("/Script/SP5V2_Game"), TEXT("ASP5V2_GameHUD"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASP5V2_GameHUD);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
