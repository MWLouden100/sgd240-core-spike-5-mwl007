// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SP5V2_GAME_SP5V2_GameHUD_generated_h
#error "SP5V2_GameHUD.generated.h already included, missing '#pragma once' in SP5V2_GameHUD.h"
#endif
#define SP5V2_GAME_SP5V2_GameHUD_generated_h

#define SP5V2_Game_Source_SP5V2_Game_SP5V2_GameHUD_h_12_RPC_WRAPPERS
#define SP5V2_Game_Source_SP5V2_Game_SP5V2_GameHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define SP5V2_Game_Source_SP5V2_Game_SP5V2_GameHUD_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASP5V2_GameHUD(); \
	friend struct Z_Construct_UClass_ASP5V2_GameHUD_Statics; \
public: \
	DECLARE_CLASS(ASP5V2_GameHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/SP5V2_Game"), NO_API) \
	DECLARE_SERIALIZER(ASP5V2_GameHUD)


#define SP5V2_Game_Source_SP5V2_Game_SP5V2_GameHUD_h_12_INCLASS \
private: \
	static void StaticRegisterNativesASP5V2_GameHUD(); \
	friend struct Z_Construct_UClass_ASP5V2_GameHUD_Statics; \
public: \
	DECLARE_CLASS(ASP5V2_GameHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/SP5V2_Game"), NO_API) \
	DECLARE_SERIALIZER(ASP5V2_GameHUD)


#define SP5V2_Game_Source_SP5V2_Game_SP5V2_GameHUD_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASP5V2_GameHUD(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASP5V2_GameHUD) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASP5V2_GameHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASP5V2_GameHUD); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASP5V2_GameHUD(ASP5V2_GameHUD&&); \
	NO_API ASP5V2_GameHUD(const ASP5V2_GameHUD&); \
public:


#define SP5V2_Game_Source_SP5V2_Game_SP5V2_GameHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASP5V2_GameHUD(ASP5V2_GameHUD&&); \
	NO_API ASP5V2_GameHUD(const ASP5V2_GameHUD&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASP5V2_GameHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASP5V2_GameHUD); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASP5V2_GameHUD)


#define SP5V2_Game_Source_SP5V2_Game_SP5V2_GameHUD_h_12_PRIVATE_PROPERTY_OFFSET
#define SP5V2_Game_Source_SP5V2_Game_SP5V2_GameHUD_h_9_PROLOG
#define SP5V2_Game_Source_SP5V2_Game_SP5V2_GameHUD_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SP5V2_Game_Source_SP5V2_Game_SP5V2_GameHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	SP5V2_Game_Source_SP5V2_Game_SP5V2_GameHUD_h_12_RPC_WRAPPERS \
	SP5V2_Game_Source_SP5V2_Game_SP5V2_GameHUD_h_12_INCLASS \
	SP5V2_Game_Source_SP5V2_Game_SP5V2_GameHUD_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SP5V2_Game_Source_SP5V2_Game_SP5V2_GameHUD_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SP5V2_Game_Source_SP5V2_Game_SP5V2_GameHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	SP5V2_Game_Source_SP5V2_Game_SP5V2_GameHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	SP5V2_Game_Source_SP5V2_Game_SP5V2_GameHUD_h_12_INCLASS_NO_PURE_DECLS \
	SP5V2_Game_Source_SP5V2_Game_SP5V2_GameHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SP5V2_GAME_API UClass* StaticClass<class ASP5V2_GameHUD>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SP5V2_Game_Source_SP5V2_Game_SP5V2_GameHUD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
