// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FVector;
struct FHitResult;
#ifdef SP5V2_GAME_SP5V2_GameProjectile_generated_h
#error "SP5V2_GameProjectile.generated.h already included, missing '#pragma once' in SP5V2_GameProjectile.h"
#endif
#define SP5V2_GAME_SP5V2_GameProjectile_generated_h

#define SP5V2_Game_Source_SP5V2_Game_SP5V2_GameProjectile_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define SP5V2_Game_Source_SP5V2_Game_SP5V2_GameProjectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define SP5V2_Game_Source_SP5V2_Game_SP5V2_GameProjectile_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASP5V2_GameProjectile(); \
	friend struct Z_Construct_UClass_ASP5V2_GameProjectile_Statics; \
public: \
	DECLARE_CLASS(ASP5V2_GameProjectile, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SP5V2_Game"), NO_API) \
	DECLARE_SERIALIZER(ASP5V2_GameProjectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define SP5V2_Game_Source_SP5V2_Game_SP5V2_GameProjectile_h_12_INCLASS \
private: \
	static void StaticRegisterNativesASP5V2_GameProjectile(); \
	friend struct Z_Construct_UClass_ASP5V2_GameProjectile_Statics; \
public: \
	DECLARE_CLASS(ASP5V2_GameProjectile, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SP5V2_Game"), NO_API) \
	DECLARE_SERIALIZER(ASP5V2_GameProjectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define SP5V2_Game_Source_SP5V2_Game_SP5V2_GameProjectile_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASP5V2_GameProjectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASP5V2_GameProjectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASP5V2_GameProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASP5V2_GameProjectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASP5V2_GameProjectile(ASP5V2_GameProjectile&&); \
	NO_API ASP5V2_GameProjectile(const ASP5V2_GameProjectile&); \
public:


#define SP5V2_Game_Source_SP5V2_Game_SP5V2_GameProjectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASP5V2_GameProjectile(ASP5V2_GameProjectile&&); \
	NO_API ASP5V2_GameProjectile(const ASP5V2_GameProjectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASP5V2_GameProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASP5V2_GameProjectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASP5V2_GameProjectile)


#define SP5V2_Game_Source_SP5V2_Game_SP5V2_GameProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CollisionComp() { return STRUCT_OFFSET(ASP5V2_GameProjectile, CollisionComp); } \
	FORCEINLINE static uint32 __PPO__ProjectileMovement() { return STRUCT_OFFSET(ASP5V2_GameProjectile, ProjectileMovement); }


#define SP5V2_Game_Source_SP5V2_Game_SP5V2_GameProjectile_h_9_PROLOG
#define SP5V2_Game_Source_SP5V2_Game_SP5V2_GameProjectile_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SP5V2_Game_Source_SP5V2_Game_SP5V2_GameProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	SP5V2_Game_Source_SP5V2_Game_SP5V2_GameProjectile_h_12_RPC_WRAPPERS \
	SP5V2_Game_Source_SP5V2_Game_SP5V2_GameProjectile_h_12_INCLASS \
	SP5V2_Game_Source_SP5V2_Game_SP5V2_GameProjectile_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SP5V2_Game_Source_SP5V2_Game_SP5V2_GameProjectile_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SP5V2_Game_Source_SP5V2_Game_SP5V2_GameProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	SP5V2_Game_Source_SP5V2_Game_SP5V2_GameProjectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	SP5V2_Game_Source_SP5V2_Game_SP5V2_GameProjectile_h_12_INCLASS_NO_PURE_DECLS \
	SP5V2_Game_Source_SP5V2_Game_SP5V2_GameProjectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SP5V2_GAME_API UClass* StaticClass<class ASP5V2_GameProjectile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SP5V2_Game_Source_SP5V2_Game_SP5V2_GameProjectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
