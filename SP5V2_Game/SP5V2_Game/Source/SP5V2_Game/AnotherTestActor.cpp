// Fill out your copyright notice in the Description page of Project Settings.


#include "AnotherTestActor.h"
#include "Components/BoxComponent.h"
#include "Uobject/ConstructorHelpers.h"
#include "Components/ArrowComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/Engine.h"
#include "Gameframework/CharacterMovementComponent.h"


// Sets default values
AAnotherTestActor::AAnotherTestActor(const FObjectInitializer& ObjectInitializer)
{	
	//this sets up a box for a trigger
	
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
	RootComponent = Mesh;
	//RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("DefaultMeshComponent"));
	VisualMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh 1"));
	TriggerVolume = CreateDefaultSubobject<UBoxComponent>(TEXT("VolumeTrigger"));
	TriggerVolume->SetBoxExtent(FVector(32.f, 32.f, 32.f));
	TriggerVolume->SetupAttachment(RootComponent);
	Arrow = CreateDefaultSubobject<UArrowComponent>(TEXT("Arrow"));
	Arrow->SetupAttachment(RootComponent);
	Arrow->SetHiddenInGame(false);

	TriggerVolume->OnComponentBeginOverlap.AddDynamic(this, &AAnotherTestActor::OnCubeBeginOverlap);

	static ConstructorHelpers::FObjectFinder<UStaticMesh> MeshToUse(TEXT("StaticMesh'/Game/Geometry/Meshes/1M_Cube.1M_Cube'"));
	//MeshComponent->SetStaticMesh(MeshToUse.Object);


	//load the sound cue
	static ConstructorHelpers::FObjectFinder<USoundCue> FireSoundCueObject(TEXT("SoundCue'/Game/FirstPerson/Audio/LaunchFireSound.LaunchFireSound'"));
	if (FireSoundCueObject.Succeeded())
	{
		FireSoundCue = FireSoundCueObject.Object;
			FirstPersonTemplateWeaponFire02 = CreateDefaultSubobject<UAudioComponent>(TEXT("FireAudioComponent"));
			FirstPersonTemplateWeaponFire02->SetupAttachment(RootComponent);
			FirstPersonTemplateWeaponFire02->SetSound(FireSoundCue);
	}

	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;	
}

void AAnotherTestActor::OnCubeBeginOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	FHitResult Hit;
	GetActorForwardVector(ENGINE_ArrowComponent_generated_h) = FVector(Arrow->GetForwardVector());
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Nyoom!"));
	if (FirstPersonTemplateWeaponFire02 && FireSoundCue)
	{
		FirstPersonTemplateWeaponFire02->Play(0.f);
	}

	{
		if (OtherActor && (OtherActor != this) && OtherComp)
			//if (Hit.GetActor()->IsRootComponentMovable())
		{
			ACharacter* MeshRootComp = Cast<ACharacter>(OtherActor);
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, OtherActor->GetName());
			if (MeshRootComp)
			{

			MeshRootComp->LaunchCharacter(Arrow->GetForwardVector() * JumpForce, false, false);
			}
			else
			{
				UProjectileMovementComponent*Prim = OtherActor->FindComponentByClass<UProjectileMovementComponent>();

				if (Prim)
				{
					Prim->Velocity = Arrow->GetForwardVector() * JumpForce;
				}
				else
				{
					GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, TEXT("NoPrim"));
				}
			}
		}

	}
}

// Called when the game starts or when spawned
void AAnotherTestActor::BeginPlay()

{
	Super::BeginPlay();
}

void AAnotherTestActor::LaunchForward()
{
	

}

// Called every frame
void AAnotherTestActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

