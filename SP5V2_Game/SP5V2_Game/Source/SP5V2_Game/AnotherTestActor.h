#pragma once
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine/StaticMeshActor.h"
#include "Components/ArrowComponent.h"
#include "Components/AudioComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Sound/SoundCue.h"
#include "GameFramework/Character.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "AnotherTestActor.generated.h"

class UBoxComponent;

UCLASS()
class SP5V2_GAME_API AAnotherTestActor : public AStaticMeshActor
{
	GENERATED_UCLASS_BODY()
	
public:	
	// Sets default values for this actor's properties
	//AAnotherTestActor(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(Category = Meshes, VisibleAnywhere)
		UBoxComponent*TriggerVolume;
		UBoxComponent*Box;
		UStaticMeshComponent*VisualMesh;
		UStaticMeshComponent*Mesh;
		UStaticMeshComponent*MeshComponent;
		UArrowComponent*Arrow;


		// called when something enters the collision bounds//
		class USoundCue* FireSoundCue;
		UFUNCTION()	
		void OnCubeBeginOverlap(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

		UPROPERTY(Category = Jump, EditAnywhere)
			float JumpForce = 10000;




protected:
	
	//AAnotherTestActor(const FObjectInitializer & ObjectInitializer);
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//adds the impluse
	
	UPROPERTY(EditAnywhere)
		float ImpulseForce;
		void LaunchForward();

		UAudioComponent* FirstPersonTemplateWeaponFire02;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
