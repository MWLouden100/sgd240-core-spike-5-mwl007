// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "SP5V2_Game.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SP5V2_Game, "SP5V2_Game" );
 