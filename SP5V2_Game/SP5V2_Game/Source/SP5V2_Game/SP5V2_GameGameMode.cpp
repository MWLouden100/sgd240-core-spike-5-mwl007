// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "SP5V2_GameGameMode.h"
#include "SP5V2_GameHUD.h"
#include "SP5V2_GameCharacter.h"
#include "UObject/ConstructorHelpers.h"

ASP5V2_GameGameMode::ASP5V2_GameGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ASP5V2_GameHUD::StaticClass();
}
