// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SP5V2_GameGameMode.generated.h"

UCLASS(minimalapi)
class ASP5V2_GameGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASP5V2_GameGameMode();
};



