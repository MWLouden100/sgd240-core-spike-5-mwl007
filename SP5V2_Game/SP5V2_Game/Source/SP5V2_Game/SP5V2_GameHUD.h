// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "SP5V2_GameHUD.generated.h"

UCLASS()
class ASP5V2_GameHUD : public AHUD
{
	GENERATED_BODY()

public:
	ASP5V2_GameHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

